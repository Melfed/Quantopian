In this bot bot, TESLA will be bought when its price is higher than its moving average * 1.03
It will put this position to zero, or sold, when it is not higher than the moving average * 1.03

It does so between January 1 2018 and April 1 2018, and does poorly especially between 26 feb and 1 march because it buys the stock after a climb of 3 days
puttin the price higher than the ma * 1.03 and then it immediately starts going down a lot after that climb, right when we bought the stock. Return = -2.51%

Changing the requirement of the price to be higher than the MA * 1.03 to MA * 1.02 is enough to go from a return of -2.51% to +5.57% as this way we'll be
taking advantage of more moments where to buy Tesla and when the stock will be going up a bit more than when we only buy it after the price being higher
than the MA * 1.03 TEST